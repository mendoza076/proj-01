#!/bin/bash

#======================
# PROJ01-1.sh - Simple Checker
# Yarelit Mendoza
# COMP 421
# Spring 2020

# remove the # on the following line to enable debugging (compare to output)
#set -o xtrace

#======================
function initialize ()
{
    # Handle user interrupts
    trap 'summarize ; exit 0' INT

    # Setup the initial variables
    # TODO variables

    # Colors
    GOOD='\033[1;32m'
    BAD='\033[1;31m'
    UGLY='\033[1;33m' # Yello
    CYAN='\033[1;36m'
    NC='\033[0m'

    num_of_runs=0
    success_runs=0
    skipped_runs=0
    failed_runs=0

    #student=""
    #lab=""

    # Check for student dir
    cd ./students || exit 2
}

#======================
function choose_student ()
{
    students=($(ls))
    PS3="Choose a student (select the number): "
    select Student in ${students[*]};do
	if [[ -z "$Student" ]]; then
	    echo "No student chosen. Peace." >&2
	    exit 1
	fi
	echo $Student
        return 0
    done
}

#======================
function choose_lab ()
{
    labs=($(ls))
    PS3="Choose a lab (select the number): "
    select Lab in ${labs[*]}; do
	if [[ -z "$Lab" ]]; then
	    echo "No lab chose. Peace." >&2
	    exit1
	fi
	echo $Lab
        return 0
    done
}
#======================
function compile ()
{
    if [[ ! -e .info ]]; then
	echo "No .info file available for lab. Exiting" >&2
	exit 1
    fi

    compile_command=$(cat .info | cut -d ":" -f1)
    eval $compile_command &> /dev/null
    if [[ ! $? -eq 0 ]]; then
	echo "Lab does not compile. Exiting" >&2
	exit 1
    fi
    return 0
}

#======================
function execute ()
{
    executable=$(cat .info | cut -d ":" -f2)
    for ex in .*
    do
	if [[ $ex == .input[0-9]* ]]; then
	    let "num_of_runs+=1"
	    echo "===========RUN $num_of_runs==========="
	    num=${ex: -1}
	    out_file=".run${num}"
	    answer_file=".output${num}"

	    if [[ ! -e $answer_file ]]; then
		echo "No matching output file for input: $ex. Skipped"
		let "skipped_runs+=1"
	    else

		eval ./$executable < $ex 1> out_file
		diff out_file $answer_file 1> /dev/null

		if [[ $? -eq 0 ]]; then
		    echo "Success"
		    let "success_runs+=1"
		else
		    echo "Failure"
		    let "failed_runs+=1"
		fi

		echo "$success_runs/$num_of_runs succeeded so far."
	    fi
	fi
    done
}

#======================
function cleanup ()
{
    for file in .*
    do
	if [[ $file == .run[0-9]* ]]; then
	    rm $file
        elif [[ -x $executable ]]; then
            rm $executable
	fi
    done
}

#======================
function summarize ()
{
    echo "=========== SUMMARY =========="
    echo "Tested $lab for $student
    	 "

    echo "Total runs: $num_of_runs"
    echo "Successful: $success_runs"
    echo "Skipped:    $skipped_runs"
    echo "Failures:   $failed_runs"

    echo "============================="
}

#======================
# Main program
initialize

### Uncomment each line as you implement each function
student=$(choose_student)
[[ $? -eq 0 ]] || exit 2
cd ./$student/labs

lab=$(choose_lab)
[[ $? -eq 0 ]] || exit 2
cd ./$lab

echo "Checking the lab $lab for student $student"

compile
execute
cleanup
summarize
